package com.w.mateusz.entity;

import java.util.Date;

public class IgnoredRequest implements GenericRequest
{
	
	private static final long serialVersionUID = -1600037177822041806L;
	private static Integer id = 0;

	private String content;
	private Date creationTime;
	
	public IgnoredRequest()
	{
		id++;
		this.content = "Default content for ignored request type. Message id " + id;
		this.creationTime = new Date();
	}

	public Integer getId()
	{
		return id;
	}

	public String getContent()
	{
		return content;
	}

	public Date getCreationTime()
	{
		return creationTime;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((creationTime == null) ? 0 : creationTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IgnoredRequest other = (IgnoredRequest) obj;
		if (content == null)
		{
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (creationTime == null)
		{
			if (other.creationTime != null)
				return false;
		} else if (!creationTime.equals(other.creationTime))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "IgnoredRequest [content=" + content + ", creationTime=" + creationTime + "]";
	}
}
