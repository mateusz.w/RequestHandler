package com.w.mateusz.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Request")
public class PersistentRequest implements GenericRequest
{

	private static final long serialVersionUID = -2362104054961970258L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column
	private String content;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationTime;

	public PersistentRequest()
	{
		this.content = "Default content for persistent type request";
		this.creationTime = new Date();
	}

	public Integer getId()
	{
		return id;
	}

	public String getContent()
	{
		return content;
	}

	public Date getCreationTimestamp()
	{
		return creationTime;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((creationTime == null) ? 0 : creationTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersistentRequest other = (PersistentRequest) obj;
		if (content == null)
		{
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (creationTime == null)
		{
			if (other.creationTime != null)
				return false;
		} else if (!creationTime.equals(other.creationTime))
			return false;
		if (id == null)
		{
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "PersistentRequest [content=" + content + ", id=" + id + ", creationTimestamp=" + creationTime
				+ "]";
	}
}
