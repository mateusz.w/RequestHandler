package com.w.mateusz.entity;

import java.util.HashMap;
import java.util.Map;

public final class RequestTypeNames
{
	public final static String PERSISTED = PersistentRequest.class.getName();
	public final static String SERIALIZED = SerializedRequest.class.getName();
	public final static String IGNORED = IgnoredRequest.class.getName();
	public final static String LOGGED = LoggedRequest.class.getName();
	
	private final Map<Integer, String> requestTypes;

	public RequestTypeNames()
	{
		requestTypes = new HashMap<>();
		requestTypes.put(1, PERSISTED);
		requestTypes.put(2, SERIALIZED);
		requestTypes.put(3, IGNORED);
		requestTypes.put(4, LOGGED);
	}

	public Map<Integer, String> getRequestTypes()
	{
		return new HashMap<>(requestTypes);
	}
}
