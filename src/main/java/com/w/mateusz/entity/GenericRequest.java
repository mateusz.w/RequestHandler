package com.w.mateusz.entity;

import java.io.Serializable;

public interface GenericRequest extends Serializable
{
	String getContent();
	Integer getId();
}
