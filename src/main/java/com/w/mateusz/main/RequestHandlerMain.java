package com.w.mateusz.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.w.mateusz.configuration.SpringConfiguration;
import com.w.mateusz.service.RequestConsumer;
import com.w.mateusz.service.RequestProducer;

public class RequestHandlerMain
{
	public static AnnotationConfigApplicationContext context;
	
	public static void main(String[] args)
	{
		initializeApplicationContext();
		
		produceAndProcessRequests(context);
	}
	
	public static void produceAndProcessRequests(ApplicationContext context)
	{
		RequestProducer producer = context.getBean("requestProducerImpl", RequestProducer.class);
		RequestConsumer consumer = context.getBean("requestConsumerImpl", RequestConsumer.class);
		consumer.processRequests(producer.generateRandomRequests(100));
	}
	
	public static void initializeApplicationContext()
	{
		context = new AnnotationConfigApplicationContext();
		context.register(SpringConfiguration.class);
		context.refresh();
	}
}
