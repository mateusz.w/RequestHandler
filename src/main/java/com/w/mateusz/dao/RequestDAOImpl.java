package com.w.mateusz.dao;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.w.mateusz.entity.PersistentRequest;

@Repository
public class RequestDAOImpl implements RequestDAO
{
	@Autowired
	SessionFactory sessionFactory;

	@Override
	@Transactional
	public void save(PersistentRequest request)
	{
		sessionFactory.getCurrentSession().save(request);
	}

}
