package com.w.mateusz.dao;

import com.w.mateusz.entity.PersistentRequest;

public interface RequestDAO
{
	void save(PersistentRequest request);

}
