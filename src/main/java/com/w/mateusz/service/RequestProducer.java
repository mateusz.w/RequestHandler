package com.w.mateusz.service;

import java.util.Queue;

import com.w.mateusz.entity.GenericRequest;

public interface RequestProducer
{
	Queue<GenericRequest> generateRandomRequests(int quantity);

}
