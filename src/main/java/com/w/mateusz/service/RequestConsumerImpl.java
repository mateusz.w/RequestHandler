package com.w.mateusz.service;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.w.mateusz.dao.RequestDAO;
import com.w.mateusz.entity.GenericRequest;
import com.w.mateusz.entity.IgnoredRequest;
import com.w.mateusz.entity.LoggedRequest;
import com.w.mateusz.entity.PersistentRequest;
import com.w.mateusz.entity.SerializedRequest;

@Component
public class RequestConsumerImpl implements RequestConsumer
{
	@Autowired
	private RequestDAO requestDAO;
	
	private static Logger logger = LogManager.getLogger(RequestConsumerImpl.class);

	@Override
	public void processRequests(Queue<GenericRequest> requests)
	{
		while(requests.peek() != null)
		{
			determineProccessingStrategy(requests.poll());
		}
		logger.info("All requests processed");
	}
	
	private void determineProccessingStrategy(GenericRequest request)
	{
		if(request.getClass() == PersistentRequest.class)
		{
			proccessPersistentRequest(request);
		}
		else if(request.getClass() == IgnoredRequest.class)
		{
			
		}
		else if(request.getClass() == SerializedRequest.class)
		{
			proccessSerializedRequest(request);
		}
		else if(request.getClass() == LoggedRequest.class)
		{
			proccessLoggedRequest(request);
		}
		else
		{
			throw new RuntimeException("There is no proccessing stragedy implemented for request type " + request.getClass().getSimpleName());
		}
	}
	
	private void proccessPersistentRequest(GenericRequest request)
	{
		requestDAO.save((PersistentRequest)request);
	}
	
	private void proccessSerializedRequest(GenericRequest request)
	{
		try (ObjectOutputStream serializer = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream("serialized.requests"))))
		{
			serializer.writeObject(request);
		}
		catch(FileNotFoundException e)
		{
			logger.error(e);
		}
		catch(IOException ex)
		{
			logger.error(ex);
		}
	}
	private void proccessLoggedRequest(GenericRequest request)
	{
		logger.info(request);
	}

}
