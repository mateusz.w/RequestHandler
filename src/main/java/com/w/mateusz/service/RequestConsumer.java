package com.w.mateusz.service;

import java.util.Queue;

import com.w.mateusz.entity.GenericRequest;

public interface RequestConsumer
{
	void processRequests(Queue<GenericRequest> requests);
}
