package com.w.mateusz.service;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

import org.springframework.stereotype.Component;

import com.w.mateusz.entity.GenericRequest;
import com.w.mateusz.entity.RequestTypeNames;
import com.w.mateusz.factory.RequestFactory;

@Component
public class RequestProducerImpl implements RequestProducer
{
	private final Map<Integer, String> requestTypeNames = new RequestTypeNames().getRequestTypes();

	@Override
	public ArrayBlockingQueue<GenericRequest> generateRandomRequests(int quantity)
	{
		ArrayBlockingQueue<GenericRequest> requests = new ArrayBlockingQueue<>(quantity);

		new Random().ints(quantity, 1, 5)
			.forEach(requestType -> requests.add(RequestFactory.getRequest(requestTypeNames.get(requestType))));

		return requests;
	}

}
