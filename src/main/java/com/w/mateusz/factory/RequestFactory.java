package com.w.mateusz.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.w.mateusz.entity.GenericRequest;

public class RequestFactory
{

	private static final Logger logger = LogManager.getLogger(RequestFactory.class);

	public static GenericRequest getRequest(String requestType)
	{

		if (requestType == null || requestType.isEmpty())
		{
			throw new IllegalArgumentException("Null or empty requestType parameter value is not valid.");
		}

		GenericRequest producedRequest = null;

		try
		{
			producedRequest = (GenericRequest) Class.forName(requestType).newInstance();
		}
		catch (InstantiationException e)
		{
			logger.error(e);
		}
		catch (IllegalAccessException e)
		{
			logger.error(e);
		}
		catch (ClassNotFoundException e)
		{
			logger.error(e);
		}

		if (producedRequest == null)
		{
			throw new RuntimeException("Request generation failed for requestType parameter value " + requestType.toString());
		}

		return producedRequest;

	}
}
