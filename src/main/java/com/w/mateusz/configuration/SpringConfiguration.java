package com.w.mateusz.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.w.mateusz")
public class SpringConfiguration
{
	
}
