package com.w.mateusz.service;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import org.junit.jupiter.api.Test;

import com.w.mateusz.entity.GenericRequest;

public class RequestConsumerImplTest
{

	@Test
	public void determineProccessingStrategy_NotSupportedRequestType_RuntimeExceptionThrown()
	{
		GenericRequest notSupportedRequestType = new GenericRequest() 
		{
			private static final long serialVersionUID = -2506821182678506109L;

			@Override
			public String getContent()
			{
				return null;
			}

			@Override
			public Integer getId()
			{
				return null;
			} 
		};
		
		Queue<GenericRequest> testQueue = new ArrayBlockingQueue<>(1);
		testQueue.add(notSupportedRequestType);
		
		
		assertThrows(RuntimeException.class, () -> new RequestConsumerImpl().processRequests(testQueue)); 
	}
}
