package com.w.mateusz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Queue;

import org.junit.jupiter.api.Test;

import com.w.mateusz.entity.GenericRequest;

public class RequestProducerImplTest
{

	@Test
	public void generateRandomRequests_checkIfReturnedQueueSizeCorrect_250()
	{
		Queue<GenericRequest> queue = new RequestProducerImpl().generateRandomRequests(250);
		
		assertEquals(queue.size(), 250);
	}
}
