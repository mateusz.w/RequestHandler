package com.w.mateusz.factory;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.w.mateusz.entity.GenericRequest;
import com.w.mateusz.entity.IgnoredRequest;
import com.w.mateusz.entity.LoggedRequest;
import com.w.mateusz.entity.PersistentRequest;
import com.w.mateusz.entity.SerializedRequest;

public class RequestFactoryTest
{
	private GenericRequest checkedRequest;
	
	@Test
	public void getRequest_WrongTypeParameter_RuntimeExceptionThrown()
	{
		assertThrows(RuntimeException.class, () -> RequestFactory.getRequest("wrongType"));
	}
	
	@Test
	public void getRequest_CorrectTypeParameter_IgnoredRequestReturned()
	{
		checkedRequest = RequestFactory.getRequest("com.w.mateusz.entity.IgnoredRequest");
		
		assertEquals(IgnoredRequest.class, checkedRequest.getClass());
	}
	
	@Test
	public void getRequest_CorrectTypeParameter_SerializedRequestReturned()
	{
		checkedRequest = RequestFactory.getRequest("com.w.mateusz.entity.SerializedRequest");
		
		assertEquals(SerializedRequest.class, checkedRequest.getClass());
	}
	
	@Test
	public void getRequest_CorrectTypeParameter_PersistentRequestReturned()
	{
		checkedRequest = RequestFactory.getRequest("com.w.mateusz.entity.PersistentRequest");
		
		assertEquals(PersistentRequest.class, checkedRequest.getClass());
	}
	
	@Test
	public void getRequest_CorrectTypeParameter_LoggedRequestReturned()
	{
		checkedRequest = RequestFactory.getRequest("com.w.mateusz.entity.LoggedRequest");
		
		assertEquals(LoggedRequest.class, checkedRequest.getClass());
	}
	
	
}
